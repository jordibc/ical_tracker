#!/usr/bin/env python

"""
Convert the database used in A Time Tracker to an iCalendar file.
"""

# See:
#   https://en.wikipedia.org/wiki/ICalendar
#   https://tools.ietf.org/html/rfc5545

import sys
from os.path import exists
import time
from socket import gethostname
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as fmt

import sqlite3


ICAL_DATETIME_FMT = '%Y%m%dT%H%M%SZ'

gid_counter = 0  # to generate unique global ids, as recommended in the RFC


def main():
    args = get_args()

    try:
        assert args.overwrite or not exists(args.output), \
            f'File already exists: {args.output}'
        assert exists(args.db), f'Cannot find database file: {args.db}'

        events = get_events(args.db)

        print(f'Writing {len(events)} events to {args.output} ...')
        with open(args.output, 'wt') as fout:
            fout.write(create_ical(events))
    except (AssertionError, sqlite3.OperationalError, IOError) as e:
        sys.exit(e)


def get_args():
    "Return the command-line arguments"
    parser = ArgumentParser(description=__doc__, formatter_class=fmt)

    add = parser.add_argument  # shortcut
    add('--db', default='timetracker.db', help='path to sqlite database file')
    add('-o', '--output', default='tracker.ics', help='name of the output file')
    add('--overwrite', action='store_true', help='overwrite ical file')

    return parser.parse_args()


def get_events(db):
    "Return a list of events [(task, dtstart, dtend)] read from database db"
    with sqlite3.connect(db) as connection:
        c = connection.cursor()
        tasks = dict(c.execute('SELECT _id,name FROM tasks'))
        return [(tasks[task_id], to_dt(start), to_dt(end))
                for task_id,start,end in c.execute('SELECT * FROM ranges')]


def to_dt(t):
    "Return an iCal datetime for the given t (in unix milliseconds time)"
    return time.strftime(ICAL_DATETIME_FMT, time.gmtime(t / 1000))


def create_ical(events):
    header = """\
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Metamagical//NONSGML An iCal Tracker v1.0//EN
""".replace('\n', '\r\n')

    footer = """\
END:VCALENDAR
""".replace('\n', '\r\n')

    return header + ''.join(create_event(*e) for e in events) + footer


def create_event(summary, dtstart, dtend):
    # TODO: Split the SUMMARY line if it extends over 75 chars, as specified
    #   in the RFC.
    return f"""\
BEGIN:VEVENT
UID:{create_global_unique_id()}
DTSTAMP:{now()}
DTSTART:{dtstart}
DTEND:{dtend}
SUMMARY:{summary}
END:VEVENT
""".replace('\n', '\r\n')


def create_global_unique_id():
    global gid_counter
    gid_counter += 1
    return f'{now()}-{gid_counter}@{gethostname()}'


def now():
    return time.strftime(ICAL_DATETIME_FMT, time.gmtime())



if __name__ == '__main__':
    main()
